import java.util.*;
class TeamIND {
	
	String pName = null;
	int jNo = 0;

	TeamIND(String pName , int jNo){
		
		this.pName = pName;
		this.jNo = jNo;
	}
	public String toString(){
		
		return pName + ":" +jNo;
	}
}

class setByName implements Comparator<TeamIND>{
	
	public int compare(TeamIND obj1, TeamIND obj2){
		
		return -(obj1.pName.compareTo(obj2.pName));
	}
}
class setByJno implements Comparator<TeamIND>{
	
	public int compare(TeamIND obj1, TeamIND obj2){

                return obj1.jNo - obj2.jNo;
        }
}
class MyLSDemo{
	
	public static void main(String[] args){
		
		ArrayList<TeamIND> al = new ArrayList<TeamIND>();

		al.add(new TeamIND("virat",18));
		al.add(new TeamIND("dhoni",7));
		al.add(new TeamIND("rohit",45));
		al.add(new TeamIND("pandya",33));


		System.out.println(al);

		Collections.sort(al,new setByName());
		System.out.println(al);
		
		Collections.sort(al,new setByJno());
		System.out.println(al);
	}
}
