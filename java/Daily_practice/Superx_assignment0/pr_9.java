
import java.util.*;
class Code09{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter String");
        String str = sc.nextLine();
        int length = str.length();

        StringBuffer sbr = new StringBuffer();
       
        for (int i = 0; i <length; i++) {
           
            char current = str.charAt(i);
               
            if(i%2==0){

                    sbr.append(Character.toUpperCase(current));
                }else{

                    sbr.append(Character.toLowerCase(current));
                }
        }

        System.out.println(sbr.toString());
    }
}