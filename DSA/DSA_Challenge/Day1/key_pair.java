class Solution {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here
                int low =0;
                int high = n-1;

                Arrays.sort(arr);

                while(low<high){
                        int sum = arr[high]+arr[low];

                        if(sum == x){
                                return true;
                        }else if(sum>x){
                                high--;
                        }else{
                                low++;
                        }
                }
                return false;

    }
}
