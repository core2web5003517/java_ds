import java.util.*;
class Code23 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr1[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr1.length;i++){
                        arr1[i]=sc.nextInt();
                }

               for(int i=0;i<arr1.length-1;i++){
                        for(int j =i+1;j<arr1.length;j++){

                                if(arr1[i]<arr1[j]){

                                        int temp = arr1[i];
                                        arr1[i]=arr1[j];
                                        arr1[j]=temp;
                                }
                        }
               }
               int mult =1;
               for(int i=0;i<3;i++){

                        mult = mult*arr1[i];
               }

               System.out.println("Output: "+mult);
        }
}
