import java.util.*;
class Employee {
	
	int EmpId = 0;
	String name = null;

	Employee(int EmpId , String name){
		
		this.EmpId = EmpId;
		this.name = name;
	}

	public String toString(){
		
		return EmpId+":"+name;
	}
}

class Demo {
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();
		al.add(new Employee(25,"kanha"));
		al.add(new Employee(15,"ashish"));
		al.add(new Employee(22,"rahul"));

		System.out.println(al);

		Collections.sort(al,(obj1,obj2)->{
			
			return ((Employee)obj1).name.compareTo(((Employee)obj2).name);

		}
			);

		System.out.println(al);

		 Collections.sort(al,(obj1,obj2)->{

                        return ((Employee)obj1).EmpId-(((Employee)obj2).EmpId);

                }
                        );

                System.out.println(al);
	}
}
