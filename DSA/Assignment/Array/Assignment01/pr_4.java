import java.util.*;
class Code4 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int max=arr[0];
                for (int i = 0; i < arr.length; i++) {

                    if(max<arr[i]){

                         max = arr[i];
                    }
                }
                int count =0;
                for (int i = 0; i < arr.length; i++) {
                    int val = arr[i];
                    for (int j = 0; j < arr.length; j++) {

                            if(val==max){

                                break;
                            }
                            count++;
                            val++;
                    }
                }

                System.out.println("Output:"+count);
     }
}
