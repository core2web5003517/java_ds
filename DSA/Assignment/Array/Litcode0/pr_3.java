import java.util.*;

class SIP{
    
    static int fun(int arr[],int x){

           int index=-1;
           for (int i = 0; i < arr.length; i++) {

                            if(arr[i]==x){
                                
                                index = i;
                                break;
                            }

                            if(arr[i]==(x-1)||arr[i]==(x+1)){

                                index = i+1;
                                break;
                            }
           }

        return index;
    }

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of array");
                int N = sc.nextInt();

                int arr[]=new int[N];

                System.out.println("Enter the elements in array");
                for (int i = 0; i < arr.length; i++) {

                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter the target");
                int x = sc.nextInt();

                int ret = fun(arr, x);

                System.out.println(ret);
        }
}
