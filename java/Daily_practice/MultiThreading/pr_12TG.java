class MyThread extends Thread {
	
	MyThread(ThreadGroup Tg, String Tn){
		
		super(Tg,Tn);
	}
	public void run(){
		
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(10);
		}catch(InterruptedException ie){
		
		}
		System.out.println(Thread.currentThread().getThreadGroup().getName());
	}
}
class ThreadGPDemo{
	
	public static void main(String[] args){
		
		ThreadGroup PThread = new ThreadGroup("INDIA");
		
		MyThread obj1 = new MyThread(PThread,"Bihar");
		obj1.start();

		MyThread obj2 = new MyThread(PThread,"Gujrat");
		obj2.start();

		MyThread obj3 = new MyThread(PThread,"Maharashtra");
		obj3.start();

		ThreadGroup CThread = new ThreadGroup(PThread,"Maharashtra");


               	MyThread obj4 = new MyThread(CThread,"Latur");
                obj4.start();


		MyThread obj5 = new MyThread(CThread,"Pune");
                obj5.start();

		MyThread obj6 = new MyThread(CThread,"Satara");
                obj6.start();
	}
}
