import java.util.*;
class Code4{
        public static void main(String [] args){
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                int product = 1;

		for(int i=0;i<arr.length;i++){
			
			product = product*arr[i];
		}

		System.out.println("product of all array elements is: "+product);

	}
}
