
import java.util.*;
class MyTreeDemo implements Comparable<MyTreeDemo> {
	
	String str = null;

	MyTreeDemo(String str){
		
		this.str = str;
	}
	public String toString(){
		
		return str;
	}
	public int compareTo(MyTreeDemo obj){
		
		return str.compareTo(obj.str);
	}
}

class TreeDemo {
	
	public static void main(String [] args){
		
		TreeSet<MyTreeDemo> ts = new TreeSet<MyTreeDemo>();

		ts.add(new MyTreeDemo("Kanha"));
		ts.add(new MyTreeDemo("Ashish"));
		ts.add(new MyTreeDemo("Rahul"));
		ts.add(new MyTreeDemo("Badhe"));


		System.out.println(ts);
	}
}
