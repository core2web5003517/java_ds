import java.util.*;
class Code24 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr1[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr1.length;i++){
                        arr1[i]=sc.nextInt();
                }

               int first = arr1[0];
               int count = 1;
               for(int i=1;i<arr1.length;i++){

                        if(arr1[i]>=first){

                                count++;
                        }
               }

               System.out.println("Output: "+count);
        }
}

