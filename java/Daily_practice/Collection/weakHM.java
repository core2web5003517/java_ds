import java.util.*;
class Demo {
	
	String str = null;

	Demo(String str){
		
		this.str = str;
	}
	public String toString(){
		
		return str;
	}
	public void finalize(){
		
		System.out.println("Notify");
	}

}
class GcDemo {
	
	public static void main(String[] args){
		
		Demo obj1 = new Demo("Shahuraj");
		Demo obj2 = new Demo("prathmesh");
		Demo obj3 = new Demo("akash");
		Demo obj4 = new Demo("atharv");

		System.out.println(obj1);
		System.out.println(obj2);
		System.out.println(obj3);
		System.out.println(obj4);
		
		//obj3 = null;
		//obj4 = null;

	        // System.gc();

		System.out.println("Starting for loop");
		for(int i =1;i<=21;i++){
			
			System.out.println("Jay shree Ram");
		}
		System.out.println("Ended for loop");

		WeakHashMap whm = new WeakHashMap();

		whm.put(obj1, 2002);
		whm.put(obj2, 2003);
		whm.put(obj3, 2004);
		whm.put(obj4, 2005);
		
		System.out.println(whm);

		obj3 = null;

		System.gc();
	

		System.out.println(whm);

		System.out.println("End main");
	}
}
