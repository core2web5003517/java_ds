import java.util.HashMap;

class WordCount {
    public static void main(String[] args) {
        String inputString = "This is a sample string. Sample string contains sample words.";
        
        // Split the input string into words
        String[] words = inputString.split("\\s+"); // Splitting by whitespace
        
        // Create a HashMap to store word count
        HashMap<String, Integer> wordCountMap = new HashMap<>();
        
        // Iterate through the words array and count occurrences of each word
        for (String word : words) {
            // Remove punctuation marks from the word
            word = word.replaceAll("[^a-zA-Z]", "").toLowerCase();
            
            // Update word count in the HashMap
            if (!word.isEmpty()) {
                wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);
            }
        }
        
        // Display the word count
        System.out.println("Word count:");
        for (String word : wordCountMap.keySet()) {
            System.out.println(word + ": " + wordCountMap.get(word));
        }
    }
}

