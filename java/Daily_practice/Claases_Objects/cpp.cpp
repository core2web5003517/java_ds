#include<iostream>

class Demo{

	int x=10;

	public:

	Demo(){

		std::cout<<"In Constructor"<<std::endl;
		Demo(10);
	}
	Demo(int x){
		
		//this();
		std::cout<<"In para"<<std::endl;
	}

	void fun(){

		std::cout<<"In fun no para"<<std::endl;
	}

	void fun(int x){

		std::cout<<"In fun int "<<std::endl;
	}
	~Demo(){

		std::cout<<"In Demo"<<std::endl;
	}
};

int main(){

	Demo obj;
	obj.fun();
	obj.fun(10);
	return 0;
}
