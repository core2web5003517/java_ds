class Outer{
	
	class Inner{
		
		void M1(){
			
			System.out.println("In Inner-M1");
		}
	}

	void M2(){
		
		System.out.println("In outer-M2");
	}
}
class Client{
	
	public static void main(String[] args){
		
		Outer obj = new Outer();
		obj.M2();

		Outer.Inner obj1 = obj.new Inner();
		obj1.M1();
	}
}
