abstract class Demo{
	
	abstract void fun();
	void gun(){
		
		System.out.println("In par gun");
	}
}
class Child extends Demo{
		
	void fun(){

		System.out.println("In child gun");
	}
}
class Client{
	
	public static void main(String[] args){
		
		Demo obj = new Child();
		obj.fun();
		obj.gun();
	}
}
