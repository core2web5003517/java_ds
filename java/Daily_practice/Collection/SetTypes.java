
import java.util.*;
class SetDemo {
	
	public static void main(String[] args){
		
		HashSet hs = new HashSet();

		hs.add(10);
		hs.add(20);
		hs.add(30);
		hs.add(40);
		hs.add(10);
		
		System.out.println("...HashSet...");
		System.out.println(hs);

		LinkedHashSet lhs = new LinkedHashSet(hs);

		System.out.println("...LinkedHashSet...");
		System.out.println(lhs);

		TreeSet ts = new TreeSet(lhs);

		System.out.println("...TreeSet...");
		System.out.println(ts);
	}
}
