import java.util.Scanner;

  class CloudChatbot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Cloud Chatbot!");

        boolean running = true;
        while (running) {
            System.out.print("User: ");
            String userInput = scanner.nextLine();

            String response = getChatbotResponse(userInput);
            System.out.println("Chatbot: " + response);

            if (userInput.equalsIgnoreCase("exit")) {
                running = false;
                System.out.println("Chatbot: Goodbye! Thanks for using Cloud Chatbot.");
            }
        }

        scanner.close();
    }

     static String getChatbotResponse(String userInput) {
        // Implement your chatbot's logic here based on user input
        // You can make API calls, process data, or provide information about cloud computing

        // Example: Responding to a specific query
        if (userInput.equalsIgnoreCase("What is cloud computing")) {
            return "Cloud computing refers to the on-demand delivery of computing services, including " +
                    "servers, storage, databases, networking, software, and analytics, over the internet.";
        }
	else if (userInput.equalsIgnoreCase("Hii")){
		return "Hello How i can help you";
	}
	else if(userInput.equalsIgnoreCase("What is your name")){
		return "My Name is chatbot";
	}
	else if(userInput.equalsIgnoreCase("How are you")){
                return "Am fine .. Thanks for Asking.....!!";
        }
	else if(userInput.equalsIgnoreCase("ok")){
                return "yeah";
        }

        // Example: Handling a generic query
        return "I'm sorry, I didn't understand your question. Can you please rephrase or ask something else?";
    }
}
