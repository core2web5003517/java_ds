
import java.util.*;
class MapUD implements Comparable{
	
	String str = null;
	int jno = 0;

	MapUD(int jno , String str){
		
		this.str = str;
		this.jno = jno;
	}
	public String toString(){
		
		return jno+":"+str;
	}
	public int compareTo(Object obj){
		
		return this.jno - ((MapUD)obj).jno;
	}

}

class HashMapUD{
	
	public static void main(String[] args){
		
		
		TreeMap tm = new TreeMap();
	
		tm.put(new MapUD(10,"sachin"),40);
                tm.put(new MapUD(12,"yuvraj"),35);
                tm.put(new MapUD(18,"virat"),33);
                tm.put(new MapUD(3,"raina"),38);
		
		System.out.println(tm);

		HashMap hm = new HashMap(tm);

		System.out.println(hm);


	}
}
