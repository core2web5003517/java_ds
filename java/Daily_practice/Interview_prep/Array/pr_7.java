import java.util.*;
class Code7{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Size of an Array :");
		int N = sc.nextInt();
		int Arr[] = new int[N];
		System.out.println("Enter the elements :");
		for(int i=0;i<N;i++){
			
			Arr[i]= sc.nextInt();
		}
		int sum =0;
		for(int i=0;i<N;i++){
			
			sum=sum+Arr[i];
		}

		System.out.println("Output :");
		System.out.println(sum);
	}
}
