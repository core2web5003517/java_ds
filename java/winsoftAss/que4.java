import java.util.HashMap;

class DuplicateCharacters {
    public static void main(String[] args) {
        String inputString = "hello world";
        
        // Create a HashMap to store character count
        HashMap<Character, Integer> charCountMap = new HashMap<>();
        
        // Convert the input string to lowercase (optional)
        inputString = inputString.toLowerCase();
        
        // Iterate through the characters of the string and count occurrences
        for (char c : inputString.toCharArray()) {
            // Ignore non-alphabetic characters
            if (Character.isLetter(c)) {
                // Update character count in the HashMap
                charCountMap.put(c, charCountMap.getOrDefault(c, 0) + 1);
            }
        }
        
        // Display duplicate characters
        System.out.println("Duplicate characters in the string:");
        for (char c : charCountMap.keySet()) {
            if (charCountMap.get(c) > 1) {
                System.out.println(c + ": " + charCountMap.get(c) + " occurrences");
            }
        }
    }
}

