class MergeArrays {

    // Function to merge two arrays X[] and Y[] into X[]
    public static void mergeArrays(int[] X, int[] Y) {
        int m = X.length; // Size of X[]
        int n = Y.length; // Size of Y[]
        
        // Move all elements of X[] to the end
        int i = m - 1;
        for (int j = m - 1; j >= 0; j--) {
            if (X[j] != 0) {
                X[i] = X[j];
                i--;
            }
        }
        
        // Merge Y[] into X[]
        int j = 0;
        int k = 0;
        i = 0;
        while (i < m && j < n) {
            if (X[i] <= Y[j]) {
                X[k++] = X[i++];
            } else {
                X[k++] = Y[j++];
            }
        }
        
        // Copy remaining elements of Y[] if any
        while (j < n) {
            X[k++] = Y[j++];
        }
    }

    public static void main(String[] args) {
        int[] X = { 0, 2, 0, 3, 0, 5, 6, 0, 0 };
        int[] Y = { 1, 8, 9, 10, 15 };
        
        mergeArrays(X, Y);
        
        // Print merged array X[]
        for (int i = 0; i < X.length; i++) {
            System.out.print(X[i] + " ");
        }
    }
}

