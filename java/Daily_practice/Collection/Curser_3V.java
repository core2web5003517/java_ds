import java.util.*;

class VectorDemo{
	
	public static void main(String[] args){
		
		Vector v = new Vector();
		
		v.add(10);
		v.add(20);
		v.add(30);
		v.add(40);
		v.add(50);

		System.out.println(v);

		ListIterator itr = v.listIterator();

		while(itr.hasNext()){
			
			System.out.println(itr.next());
		}

		 while(itr.hasPrevious()){

                        System.out.println(itr.previous());
                }
		
	       	System.out.println(v);

		Enumeration cursor = v.elements();

		System.out.println(cursor.getClass().getName());

		 while(cursor.hasMoreElements()){

                        System.out.println(cursor.nextElement());
                }

		System.out.println(v);
	}
}
