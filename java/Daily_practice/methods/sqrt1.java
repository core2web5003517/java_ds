import java.util.*;

class SqrtDemo{
	
	static int sqrt(int num){
			
		int start = 1;
		int end = num;
		int ans =0;
		int itr =0;
		while(start<=end){
			
			itr++;
			int mid = (start+end)/2;
			int sq = mid * mid;

			if(sq == num){
				
				System.out.println("itr:"+itr);	
				return mid;
			}

			if(sq > num){
				
				end = mid-1;
			}else{
				
				ans = mid;
				start = mid +1;
			}
		}
		
		System.out.println("itr:"+itr);
		return ans;
	}

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		int ret = sqrt(num);
		System.out.println(ret);
	}
}
