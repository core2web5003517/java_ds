import java.io.*;
class Code28{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Size: ");
		int N = Integer.parseInt(br.readLine());
		int arr[] = new int[N];

		System.out.println("Enter the elements in Array: ");

		for(int i =0;i<arr.length;i++){
			
			arr[i]= Integer.parseInt(br.readLine());
		}

		int n = arr.length;

        	for (int i = 0; i < n; i++) {
            		if (arr[i] != Integer.MIN_VALUE) {
                		System.out.print(arr[i] + " ");

                		for (int j = i + 1; j < n; j++) {
                    			if (arr[i] == arr[j]) {
                        		arr[j] = Integer.MIN_VALUE;
                    			}
                		}
            		}

		}

		System.out.println();
	}
}
