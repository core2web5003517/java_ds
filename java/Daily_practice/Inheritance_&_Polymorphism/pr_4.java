class Parent{
	
	int x = 10;
	static int y = 20;

	Parent(){
		System.out.println("IN Parent Constr");
	}
	void Access(){
		 System.out.println(super.hashCode());
	}
}
class Child extends Parent {
		
	int x = 100;
	static int y = 200;

	Child(){

		System.out.println("IN Child Constr");
	}
	void Display(){
		
		System.out.println(super.hashCode());
	}
}
class Client{
	
	public static void main(String args[]){
		
		Child obj = new Child();
		System.out.println(obj.hashCode());
		Parent obj1 = new Parent();
		System.out.println(obj1.hashCode());
		System.out.println("METHOD");
		obj1.Access();
		obj.Access();
		obj.Display();
	}
}
