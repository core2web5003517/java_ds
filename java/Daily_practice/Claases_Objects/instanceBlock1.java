class InstB{
	
	InstB(){
		
		int x=10;
		System.out.println("IN Constructor");
		
	}
	{
		int y=20;
	  System.out.println("Instance block1");
	    System.out.println(y);

	}

	public static void main(String [] args){
		
		int z=30;
		System.out.println("IN main");
		System.out.println(z);
		InstB obj = new InstB();

	}

	{

		System.out.println("Instance block2");
	}
	static{

	  System.out.println("IN static block");
	}
}
