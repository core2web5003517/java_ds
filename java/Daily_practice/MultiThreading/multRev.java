class Virat extends Thread {

	Virat(String str){
		
		super(str);
	}
	
	public void run(){
		
		System.out.println(Thread.currentThread().getName());
		System.out.println(Thread.currentThread().getPriority());

	}
}

class Cricket {
	
	public static void main(String[] args){
		
		Virat vk = new Virat("Prem");
		vk.start();

		System.out.println(vk.getName());

		vk.setName("saroj");
		vk.setPriority(5);
		
		System.out.println(Thread.currentThread().getName());
		System.out.println(vk.getName());

		/*for(int i=0;i<10;i++){
			
			System.out.println();
		}*/

		System.out.println("End");
	}
}
