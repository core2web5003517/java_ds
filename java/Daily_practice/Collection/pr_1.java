import java.util.*;

class ArrayListDemo extends ArrayList {
	
	public static void main(String[] args){
	ArrayListDemo al = new ArrayListDemo();

	al.add(10);
	al.add(30);
	al.add(20);
	al.add("Shahu");
	al.add(10);
	al.add(18);

	System.out.println(al);

	System.out.println(al.set(2,"Lohakare"));

	System.out.println(al);

	al.remove(1);

        System.out.println(al);

	al.add(2,"Shahuraj");

	System.out.println(al);

	al.remove(3);

	System.out.println(al);

	System.out.println(al.hashCode());

	System.out.println(al);

	System.out.println(al.indexOf("Shahuraj"));
	System.out.println(al.equals(10,"Lohakare","Shahuraj",10,18));


	}
}
