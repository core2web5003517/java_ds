import java.util.*;
class Code21{
        public static void main(String [] args){
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
		
		int mincount = 1;
		int index = -1;
                for(int i=0;i<arr.length-1;i++){
			int count =1;
			for(int j=i+1;j<arr.length;j++){
				
				if(arr[i]==arr[j]){

					count++;
				}
			}

			if(count<=mincount){
				
				mincount=count;
				index = i;
			}
		}
		
		if(index == -1){
			
			System.out.println("Output: 0");
		}else{	
			System.out.println("Output: "+arr[index]);
		}
        }
}
