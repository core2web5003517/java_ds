import java.util.*;
class Code25 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int n = arr.length;
		int fproduct =1;
		for (int i = 0; i < n - 1; i++) {
			int product =1;
            		for (int j = i + 1; j < n; j++) {
                			
					product = arr[i]*arr[j];
					if(product > fproduct){
						
						fproduct = product;
					}
                		}
            		}

		System.out.println("output: "+fproduct);
    	}
}
