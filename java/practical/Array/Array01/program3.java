//WAP to take size of array from user and also take an integer element from user print product of odd index only.
import java.io.*;
class Code3{
	public static void main(String[] shahu)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size :");
	int N=Integer.parseInt(br.readLine());
	int arr[]=new int[N];
	System.out.println("Enter Elements:");
	int product=1;
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
		if(i%2!=0){
			product=product*arr[i];
		}
	}
		System.out.println("product of odd index elements is = "+product);
	}

}
