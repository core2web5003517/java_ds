class ThisDemo{
	
	int x = 20;
	ThisDemo(){
	
		 System.out.println(this(10));
		System.out.println("IN 1");
	}
	ThisDemo(int y){
			
		 this();
		 System.out.println("IN 2");
		 System.out.println(y);
		 System.out.println(this.x);
	}

	public static void main(String[] args){
		
		ThisDemo obj = new ThisDemo(10);
		ThisDemo obj1 = new ThisDemo();
	}
}
