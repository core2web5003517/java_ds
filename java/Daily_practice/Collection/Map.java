import java.util.*;
class Demo {
	
	public static void main(String[] args){
		
		LinkedHashMap hm = new LinkedHashMap();
		hm.put(130,"sachin");
		hm.put(220,"kishya");
		hm.put(300,"rahul");
		hm.put(400,"kanha");
		hm.put(220,"pishya");

		System.out.println(hm);


		IdentityHashMap lhm = new IdentityHashMap();
                
		lhm.put(130,"sachin");
                lhm.put(220,"kishya");
		lhm.put(220,"rupya");
                lhm.put(300,"kanha");
                lhm.put(200,"rahul");

		System.out.println(lhm);

		SortedMap sm = new TreeMap();
                sm.put(10,"sachin");
                sm.put(20,"kishya");
                sm.put(30,"rahul");
                sm.put(40,"kanha");
                sm.put(20,"pishya");

		System.out.println(sm);
	}
}
