import java.io.*;
class Code1{
	public static void main(String[] args)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size of Array : ");
	int N = Integer.parseInt(br.readLine());
	int arr[]= new int [N];
	System.out.println("Enter the elements in array");
	int sum=0;
	for(int i=0; i<arr.length; i++){
		arr[i]=Integer.parseInt(br.readLine());
		sum=sum+arr[i];
	   }
		System.out.println("The sum of all elements in array is : " + sum);
	}
}
