import java.util.*;
class Code6{
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the String : ");
		String str1 = sc.next();
		StringBuffer str2 = new StringBuffer(str1);
		str2.reverse();
		String str = str2.toString();
		System.out.println("Reverse String : "+str);
	}
}
