import java.io.*;
class Code9{

	public static void main(String[] args)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size of Array : ");
	int N = Integer.parseInt(br.readLine());

	int arr[] =new int[N];
	System.out.println("Enter the elements in Array :");
		
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());	
		}

	System.out.println("Output : ");
       	if(N>2){
	     for(int i=0;i<arr.length;i++){
		for(int j=i+1;j<arr.length;j++){
			if(arr[i]>arr[j]){
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
	     }
	}else{
		System.out.println("you have entered size of " + N);
	}

	System.out.println("second largest num: "+arr[arr.length-2]);


	}
}

