//WAP to take size of array from user and also take an integer element from user print sum of odd elements only.
import java.io.*;
class Code1{
	public static void main(String[] shahu)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size :");
	int N=Integer.parseInt(br.readLine());
	int arr[]=new int[N];
	System.out.println("Enter Elements:");
	int sum=0;
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());
		if(arr[i]%2!=0){
			sum=sum+arr[i];
		}
	}
		System.out.println("Sum of odd elements is = "+sum);
	}

}
