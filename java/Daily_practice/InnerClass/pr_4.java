class Outer{
	
	void m1(){
		
		System.out.println("In M1-outer");
	}

	static class Inner{
		
		void m1(){
			 
			System.out.println("In M1-Inner");
		}
	}
}
class client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer.Inner();
		obj.m1();
	}
}
