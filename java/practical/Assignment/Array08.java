import java.util.*;
class Code8{
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of Array : ");
		int N = sc.nextInt();
		int arr[]=new int[N];
		System.out.println("Enter the elements in Array : ");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the target : ");
		int Num = sc.nextInt();
		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]+arr[j]==Num){
					System.out.println(i+","+j);
				}
			}
		}

	}
}
