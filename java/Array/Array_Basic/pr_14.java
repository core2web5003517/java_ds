import java.util.*;
class Code14{
        public static void main(String [] args){
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                
		 int maxCount = 0; 
        	 int maxNumber = 0;
		 int n = arr.length; 

        	for (int i = 0; i < n; i++) {
            		int index = arr[i] % n;
            		arr[index] += n;

           		 // Update maximum repeating number if needed
            		if (arr[index] / n > maxCount) {
                	maxCount = arr[index] / n;
                	maxNumber = arr[i] % n;
            		}		
        	}


		System.out.println("max repeated no. is: "+maxNumber);

        }
}
