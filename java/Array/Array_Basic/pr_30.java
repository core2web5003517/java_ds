import java.util.*;
class Code30 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

		System.out.println("Enter the number target: ");
		int target = sc.nextInt();
		

		 int closest = arr[0];
        	 int minDiff = Math.abs(arr[0] - target);

        	for (int i = 1; i < arr.length; i++) {
            		int diff = Math.abs(arr[i] - target);

            		if (diff < minDiff || (diff == minDiff && arr[i] > closest)) {
                		closest = arr[i];
                		minDiff = diff;
            		}

		}

		System.out.println(closest);
    	}
}
