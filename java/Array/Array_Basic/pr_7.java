import java.util.*;

class Code7 {

	public static void main(String[] args) {
            
        Scanner sc = new Scanner(System.in);

	System.out.println("enter array size: ");
        int size = sc.nextInt();

        int[] arr1  = new int[size];
	System.out.println("enter array elements: ");
	for(int i=0;i<arr1.length;i++){

            arr1[i] = sc.nextInt();

        }

	for(int i=0;i<arr1.length;i++){

            for(int j=i+1;j<arr1.length;j++){

                if(arr1[i]<arr1[j]){
                int temp = arr1[i];
                arr1[i]=arr1[j];
                arr1[j]=temp;
                }
            }
        }

        for(int i=0;i<arr1.length;i++){

            System.out.print(arr1[i]);
        }
       System.out.println();
    }
}
