import java.util.*;

class Code1{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Size:");
		int N = sc.nextInt();
		int arr[]= new int[N];

		System.out.println("Enter the elements:");
		for(int i=0;i<arr.length;i++){
			
			arr[i]= sc.nextInt();
		}

		int Leftmax[] = new int [arr.length];
		Leftmax[0]=arr[0];
	
		for(int i=1;i<arr.length;i++){
			
			if(Leftmax[i-1]<arr[i]){
				
				Leftmax[i] = arr[i];
			}else{
				
				Leftmax[i]= Leftmax[i-1];
			}
		}

		for(int i=0;i<Leftmax.length;i++){
			
			System.out.print(Leftmax[i]+" ");
		}

		System.out.println();
		
	}
}
