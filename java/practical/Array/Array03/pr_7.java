import java.io.*;
class Code7{

	public static void main(String[] args)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size of Array : ");
	int N = Integer.parseInt(br.readLine());

	int arr[] =new int[N];
	System.out.println("Enter the elements in Array :");
		
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());	
		}

	System.out.println("Output : ");
	for(int i=0;i<arr.length;i++){
			int sum=0;
			int temp=arr[i];
			while(arr[i]!=0){
				int rem=arr[i]%10;
				int fact=1;
				for(int j=1;j<=rem;j++){
					
			   		fact=fact*j;
				}

				sum = sum+fact;
				arr[i]=arr[i]/10;
			}
			if(temp==sum){	
			System.out.println("strong no "+ temp + " found at index "+ i);
			}
			
		}
	}
}

