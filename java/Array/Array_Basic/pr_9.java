import java.util.*;
class Code9 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter an index which want to delete: ");
                int index = sc.nextInt();

                for(int i =0;i<arr.length;i++){

                        if(i==index){
                            continue;
                        }
                        System.out.print(arr[i]+" ");
                }
          
	  	System.out.println();
       }
}
