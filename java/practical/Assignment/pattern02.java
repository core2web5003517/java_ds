import java.io.*;
class Code2{
	public static void main(String [] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the no. of Rows : ");
		int N = Integer.parseInt(br.readLine());
		int num=N;
		int ch=num+65;
		int ch1=num+93;
		int space=0;
		for(int i=1;i<=N;i++){
			for(int j=1;j<=space;j++){
				System.out.print(" ");
			}
			for(int j=1;j<=N-i+1;j++){
				if(j%2==0){
					System.out.print((char) ch1+" ");
					ch1++;
				}
				else{
					System.out.print((char) ch+" ");
					ch--;
				}
			}

			 System.out.println();
			 space +=2;
			 
		}
	}
}
