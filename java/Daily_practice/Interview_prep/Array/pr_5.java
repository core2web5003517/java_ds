import java.util.*;
class Code5{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Size of array : ");
		int N = sc.nextInt();
		int Arr[] = new int[N];
		System.out.println("Enter the elements : ");
		for(int i=0;i<N;i++){
			
			Arr[i]=sc.nextInt();
		}
		int Max = Arr[0];
		for(int i=0;i<N;i++){
			
			if(Arr[i]>Max){
				
				Max=Arr[i];
			}
		}

		System.out.println("Output :");
		System.out.println(Max);
	}
}
