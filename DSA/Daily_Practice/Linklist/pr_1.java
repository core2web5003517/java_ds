import java.util.Scanner;

class Node{

    int data;
    Node next = null;

    Node(int data){

        this.data = data;
    }
 }

 class Linklist{

    Node head = null;

    //addnode

    void addNode(int data){

            Node newNode = new Node(data);
            if(head == null)
                    head = newNode;
            else{

                Node temp = head;

                while (temp.next!=null) {
                    
                        temp = temp.next;
                }

                temp.next = newNode;
            }
    }

        void printLL(){

            if(head == null){

                System.out.println("Empty Linklist");
                return;
            }else{

                Node temp = head;
                while(temp.next!=null){

                    System.out.print(temp.data + "->");
                    temp = temp.next;
                }

                System.out.println(temp.data);
            }
        }

        void reverseItr(){
  		
      		if(head==null){
			
			return;
		}

		Node prev = null;
		Node curr = head;
		Node forward = null;

		while(curr!=null){
			
			forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}

		head = prev;
        }
 }

 class client{

    public static void main(String[] args) {
        
            Linklist ll = new Linklist();
            Scanner sc = new Scanner(System.in);
            char ch;

            do{

                System.out.println("1.addNode");
                System.out.println("2.printLL");
                System.out.println("3.reverseItr");
                System.out.println("4.reverseRec");

                System.out.println("Enter your choice");
                int choice = sc.nextInt();

                switch (choice) {
                    case 1:

                            {

                                System.out.println("Enter data");
                                int data = sc.nextInt();
                                ll.addNode(data);
                            }
                        
                        break;

                    case 2:
                            {

                                ll.printLL();
                            }
                            break;

                    case 3:
                    {

                            ll.reverseItr();
                    }
                    break;
                
                    default:

                            System.out.println("Wrong choice");
                            
                        break;
                }

                System.out.println("Do you want to continue?");
                ch = sc.next().charAt(0);

            }while(ch=='y'|| ch=='Y');
    }
 }
