class MaximumSumPath {
    // Function to find the maximum sum path involving elements of two sorted arrays
    public static int maxSumPath(int[] X, int[] Y) {
        int m = X.length; // Size of array X
        int n = Y.length; // Size of array Y
        
        int i = 0, j = 0; // Pointers for arrays X and Y respectively
        int sumX = 0, sumY = 0; // Variables to store cumulative sum for each array
        int result = 0; // Variable to store the maximum sum path
        
        // Loop until both arrays are processed
        while (i < m && j < n) {
            // If the current element in X is smaller, add it to sumX
            // Else, add the current element in Y to sumY
            if (X[i] < Y[j]) {
                sumX += X[i++];
            } else if (X[i] > Y[j]) {
                sumY += Y[j++];
            } else { // If both elements are equal (common element)
                // Add the maximum of sumX and sumY to the result
                result += Math.max(sumX, sumY);
                
                // Reset sumX and sumY to the current common element
                sumX = 0;
                sumY = 0;
                
                // Move to the next element in both arrays
                i++;
                j++;
            }
        }
        
        // Add remaining elements of X to sumX
        while (i < m) {
            sumX += X[i++];
        }
        
        // Add remaining elements of Y to sumY
        while (j < n) {
            sumY += Y[j++];
        }
        
        // Add the maximum of sumX and sumY to the result
        result += Math.max(sumX, sumY);
        
        return result;
    }

    public static void main(String[] args) {
        int[] X = { 3, 6, 7, 8, 10, 12, 15, 18, 100 };
        int[] Y = { 1, 2, 3, 5, 7, 9, 10, 11, 15, 16, 18, 25, 50 };
        
        System.out.println("Maximum sum path is: " + maxSumPath(X, Y));
    }
}

