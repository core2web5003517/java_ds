class NPEDemo{
	
	void m1(){
		
		System.out.println("in m1");
	}
	void m2(){
		
		System.out.println("in m2");
	}

	public static void main(String[] args){
		
		System.out.println("start of main");
		NPEDemo obj = new NPEDemo();
		obj.m1();
		obj = null;
		try{
		obj.m2();
		}catch(NullPointerException obj1){

		}
		System.out.println("End of main");
	}
}
