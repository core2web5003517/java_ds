import java.util.*;
class DataOverFlowException extends RuntimeException{
	
	DataOverFlowException(String msg){
		
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException{

        DataUnderFlowException(String msg){

                super(msg);
        }
}
class Client{
	
	public static void main(String args[]){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array : ");
		int N = sc.nextInt();
		int arr[] = new int[N];
		System.out.println("Enter the integer data");
		System.out.println(" 0 < data < 100 ");
		for(int i=0; i<arr.length;i++){

			int Data = sc.nextInt();
		try{		
		if(Data<0){
		throw new DataUnderFlowException("mitra data zero peksha mota taak");
		}
		}catch(DataUnderFlowException obj){
			
			obj.printStackTrace();
		}
		if(Data>100)
		throw new DataOverFlowException("mitra data 100 peksha lahan taak");	

		arr[i]=Data;
	
		}

		for(int i=0;i<arr.length;i++){
			
			System.out.print(arr[i]+" ");
		}

		System.out.println();
	}
}
