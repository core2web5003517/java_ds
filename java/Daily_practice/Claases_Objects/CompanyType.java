class Player{
	
	private int JerNo = 0;
	private String name =null;
	String Pname = "Virat";

	Player(int JerNo, String name){
		
		this.JerNo = JerNo;
		this.name = name;
		System.out.println("in constructor 1");
	
	}

	void info(){

		System.out.println(JerNo + " = " + name);
		System.out.println(this.name);
                System.out.println(System.identityHashCode(this.name));
	}
	
}

class Client{
	public static void main(String[] args){
		
		
		Player obj1 = new Player(18,"Virat");
		obj1.info();

		System.out.println(System.identityHashCode(obj1.Pname));
		
		Player obj2 = new Player(7,"MSD");
                obj2.info();
		
		Player obj3 = new Player(18,"Virat");
                obj3.info();

		System.out.println(System.identityHashCode(obj3.Pname));
	}
}
