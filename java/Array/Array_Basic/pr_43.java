import java.util.*;
class Code43 {

        static int countElements(int[] arr, int num1, int num2) {
       
	int n = arr.length;
        int count = 0;
        boolean num1Found = false;
	boolean num2Found = false;
        int num1Index = -1;
        int num2Index = -1;

        for (int i = 0; i < n; i++) {
            if (arr[i] == num1) {
                num1Index = i;
                num1Found = true;
                break;
            }
        }

        for (int i = n - 1; i >= 0; i--) {
            if (arr[i] == num2) {
                num2Index = i;
	        num2Found = true;
                break;
            }
        }
	
	if(!(num1Found && num2Found)){
		
		System.out.println("Invalid input : plzz check the values of num1 and num2 ...");

	}else {
            
	    	count = num2Index - num1Index - 1;
        }

	return count;

        
    }

    public static void main(String[] args) {
        
	    Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter num1: ");
                int num1 = sc.nextInt();
                System.out.println("Enter num2: ");
                int num2 = sc.nextInt();

        	int count = countElements(arr, num1, num2);
       		System.out.println("Count is: "+count);

    }

}

