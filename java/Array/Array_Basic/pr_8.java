import java.util.*;
class Code8 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
        
        // Sort the array
          for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }

        int count = 1;
        System.out.print("Elements with Even occurrences: ");

        int n = arr.length;
	boolean hasEvenOccurrences = false;
        for (int i = 1; i < n; i++) {
            if (arr[i] == arr[i - 1]) {
                count++;
            } else {
                if (count % 2 == 0) {
                    System.out.print(arr[i - 1] + " ");
		     hasEvenOccurrences = true;
                }
                count = 1;
            }
        }

        // Check the last element
        if (count % 2 == 0) {
            System.out.print(arr[n - 1]);
	     hasEvenOccurrences = true;
        }

	 if (!hasEvenOccurrences) {
            System.out.print("-1");
        }

	System.out.println();
    }
}
