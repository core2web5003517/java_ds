class Singleton{
	
	static Singleton obj = new Singleton();

	private Singleton(){
		

	}

	static Singleton GetObject(){
		
		return obj;
	}
}
class Client{
	
	public static void main(String[] args){
		
		Singleton obj1 = Singleton.GetObject();
		System.out.println(obj1);


                Singleton obj2 = Singleton.GetObject();
                System.out.println(obj2);
	}
}
