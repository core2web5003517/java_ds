import java.util.*;
class Code21 {
    public static int firstElementOccursKTimes(int[] arr, int N, int K) {
        for (int i = 0; i < N; i++) {
            int count = 0;

            for (int j = 0; j < N; j++) {
                if (arr[j] == arr[i])
                    count++;

                if (count == K)
                    return arr[i];
            }
        }

        return -1; 
    }

    public static void main(String[] args) {
         Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter k: ");
                int k = sc.nextInt();
                int n = arr.length;
        	int result = firstElementOccursKTimes(arr, n, k);
        	System.out.println("Output: " + result); 
    }
}
