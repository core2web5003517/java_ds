import java.util.*;
class factorial{

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the number");
                int num = sc.nextInt();

                int rev =0;

                while (num!=0) {

                        int rem = num%10;
                        rev = rev*10+rem;
                        num = num/10;
                }

                while (rev!=0) {

                        int rem = rev%10;
                        if(rem%2==0){

                            int fact=1;
                            for (int i = 1; i <=rem; i++) {

                                fact = fact*i;
                            }

                            System.out.print(fact+" ");
                        }

                        rev = rev/10;
                }

                System.out.println();
        }
}