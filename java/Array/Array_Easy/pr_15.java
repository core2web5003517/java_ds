import java.util.*;
class Code15 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr1[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr1.length;i++){
                        arr1[i]=sc.nextInt();
                }

                System.out.println("Enter the size of 2ndArray : ");
                int N1 = sc.nextInt();
                int arr2[]=new int[N1];
                System.out.println("Enter the elements in 2ndArray : ");
                for(int i=0;i<arr2.length;i++){
                        arr2[i]=sc.nextInt();
                }
                 
                int n = arr1.length;
                int m = arr2.length;
                int i = 0, j = 0;

        while (i < n && j < m) {
            if (arr1[i] < arr2[j]) {
                if (i == 0 || arr1[i] != arr1[i - 1])
                    System.out.print(arr1[i] + " ");
                i++;
            } else if (arr1[i] > arr2[j]) {
                if (j == 0 || arr2[j] != arr2[j - 1])
                    System.out.print(arr2[j] + " ");
                j++;
            } else {
                if (i == 0 || arr1[i] != arr1[i - 1])
                    System.out.print(arr1[i] + " ");
                i++;
                j++;
            }
        }

        while (i < n) {
            if (i == 0 || arr1[i] != arr1[i - 1])
                System.out.print(arr1[i] + " ");
            i++;
        }

        while (j < m) {
            if (j == 0 || arr2[j] != arr2[j - 1])
                System.out.print(arr2[j] + " ");
            j++;
        }

		System.out.println();
      }
}
