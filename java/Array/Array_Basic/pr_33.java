import java.util.*;
class Code33{
        public static void main(String [] args){
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int sum1 =0;
                int sum2 =0;
                for(int i=0;i<arr.length;i++){
                        
                        if(i<arr.length/2){
                                sum1 = sum1+arr[i];
                        }else{
                                sum2=sum2+arr[i];
                        }
                }

                System.out.println("output: "+sum1*sum2);
                
        }
}
