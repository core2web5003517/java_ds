import java.util.*;

class Reverse{
    
    static int Reversenum(int x){

            int rev =0;

            if((x<=-231) || (x>=230)){

                return 0;
            }
            while(x!=0){

                    int rem = x%10;
                    rev = rev*10+rem;
                    x=x/10;
            }

            return rev;
    }

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                int num = sc.nextInt();

                int ret = Reversenum(num);

                System.out.println(ret);
        }
}
