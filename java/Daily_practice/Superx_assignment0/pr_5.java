import java.util.Scanner;

 class ReverseSumArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        int count =0;
        int reversedNumber = 0;
        while (number != 0) {
            int digit = number % 10;
            reversedNumber = reversedNumber * 10 + digit;
            count++;
            number /= 10;
        }

        int length = count;
        int[] reversedSums = new int[length];

        int temp = reversedNumber;
        int[]arr = new int[count];
        int i=0;
        while(temp!=0){

                int rem = temp%10;
                for (; i < arr.length;) {
                        
                        arr[i]=rem;
                        i++;
                        break;
                }
                temp=temp/10;
        }
        
        int flag =0;
        for (int j = 0; j < reversedSums.length; j++) {
            int digit = reversedNumber % 10;
            if(flag==0){
                reversedSums[0]=digit;
                flag=1;
            }else{

                 reversedSums[j] = arr[j-1]+digit;
            }
            reversedNumber /= 10;
           
           
        }

        System.out.print("Output: ");
        for (int k = reversedSums.length - 1; k >= 0; k--) {
            System.out.print(reversedSums[k] + " ");
        }
    }
}
