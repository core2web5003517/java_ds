abstract class Parent {
	
	 abstract void fun();
	 void gun(){

                 System.out.println("In parent gun ");
         }
}
class Child extends Parent{
	
	 void fun(){
		
		 System.out.println("In child Fun ");
	 }


}
class Client{
	
	public static void main(String[] args){
	
		Parent obj = new Child();
		obj.fun();
		obj.gun();
	}
}
