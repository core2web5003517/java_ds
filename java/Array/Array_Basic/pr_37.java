import java.util.*;
class Code37 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int n = arr.length;
                int left = 0;

        
                for (int i = 0; i < n; i++) {
                         if (arr[i] < 0) {
                        
                                 int temp = arr[i];
                                 arr[i] = arr[left];
                                 arr[left] = temp;
                                 left++;
                         }
                 }

                 for(int i=0;i<arr.length;i++){

                        System.out.print(arr[i]+" ");
                 }

                 System.out.println();
        }
}
