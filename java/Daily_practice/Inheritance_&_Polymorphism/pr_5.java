class Dad{
	
	int x = 10;
	static int y = 20;

	Dad(){
		
		System.out.println("in Dad Constructor");
	}
	void property(){
		
		 System.out.println("Dad's property");
	}
}
class Son extends Dad{
	
	int x = 100;
	static int y = 200;

	Son(){
	
	 System.out.println(super.hashCode());
                 System.out.println(this.hashCode());
		 System.out.println("in Son constructor");
	}
	   void property(){

                 System.out.println("sons property");
        }
	void Display(){
		
		 System.out.println(super.x);
		 System.out.println(super.y);
		 property();
		 super.property();
		 System.out.println(super.hashCode());
		 System.out.println(this.hashCode());
	}

	Son(int x){
		
		 System.out.println("in Son constructor 2");
	}

}

class Info{
	
	public static void main(String [] Shau){
		
		Son obj = new Son();
		obj.Display();
	}
}
