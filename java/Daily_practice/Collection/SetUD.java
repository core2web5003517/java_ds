import java.util.*;

class MyDemo { 
	
	
	int jerNo = 0;
	String pName = null;

	MyDemo(int jerNo , String pName){
		
		this.jerNo = jerNo;
		this.pName = pName;
	}

	public String toString(){
		
		return jerNo+":"+pName;
	}
		
}

class setByName implements Comparator<MyDemo>{
	
	public int compare(MyDemo obj1,MyDemo obj2){
		
		return obj1.pName.compareTo(obj2.pName);
	}

}
class MySetDemo {
	
	public static void main(String[] args){
		
		ArrayList<MyDemo> al = new ArrayList<MyDemo>();
		al.add(new MyDemo(18,"Virat"));
		al.add(new MyDemo(7,"MsDhoni"));
		al.add(new MyDemo(45,"Rohit"));
		al.add(new MyDemo(18,"Virat"));

		System.out.println(al);

		Collections.sort(al,new setByName());

		System.out.println(al);

	}
}
