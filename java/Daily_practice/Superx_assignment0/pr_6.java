import java.util.*;
class Code06{

        public static void main(String[] args) {
                
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter num");
                int num = sc.nextInt();
                int target = Integer.MIN_VALUE;
                int temp =num;
                while(temp!=0){

                    int rem = temp%10;
                   
                    if(rem>target){
                       
                        target=rem;
                    }
                    
                    temp = temp/10;
                }

                System.out.println("Output:"+target);
        }
}
