import java.io.*;
class Code1{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Array Size: ");
		int N = Integer.parseInt(br.readLine());
		int arr[] = new int[N];

		System.out.println("Enter the elements in Array: ");

		for(int i =0;i<arr.length;i++){
			
			arr[i]= Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the number to be Searched: ");
		int num = Integer.parseInt(br.readLine());

		for(int i =0;i<arr.length;i++){
			
			if(arr[i]==num){
				
				System.out.println("number is present at Index: "+i);
			}
		}
	}
}
