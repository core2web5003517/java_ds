import java.io.*;
class Code6{

	public static void main(String[] args)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size of Array : ");
	int N = Integer.parseInt(br.readLine());

	int arr[] =new int[N];
	System.out.println("Enter the elements in Array :");
		
	for(int i=0;i<arr.length;i++){
		arr[i]=Integer.parseInt(br.readLine());	
		}

	System.out.println("Output : ");
	for(int i=0;i<arr.length;i++){
			int rev=0;
			int temp=arr[i];
			while(arr[i]!=0){
				int rem=arr[i]%10;
				rev = rev*10+rem;
				arr[i]=arr[i]/10;
			}
			if(temp==rev){	
			System.out.println("palindrome no "+ temp + " found at index "+ i);
			}
			
		}
	}
}

