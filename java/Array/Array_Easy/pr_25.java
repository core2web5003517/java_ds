import java.util.*;
class Code25 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr1[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr1.length;i++){
                        arr1[i]=sc.nextInt();
                }

               for(int i =0;i<arr1.length-1;){

                        if(arr1[i]%2!=0){

                                int temp = arr1[i];
                                arr1[i]=arr1[i+1];
                                arr1[i+1]=temp;
                        }

                        i=i+2;
               }

               for (int i = 0; i < arr1.length; i++) {

                        System.out.print(arr1[i]+" ");
               }

               System.out.println();
               int countNeeded = arr1.length/2;
               int count =0;
               for (int i = 0; i < arr1.length; i++) {

                        if((i%2==0)&& (arr1[i]%2==0)){

                                count++;
                        }
               }

               if(count==countNeeded){

                        System.out.println("Output:1");
               }else{

                        System.out.println("Output:0");
               }
        }
}
