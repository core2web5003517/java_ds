class XThread implements Runnable{
	
	public void run(){
		
		System.out.println("IN RUN");
		System.out.println(Thread.currentThread().getName());
	}
}
class YThread{
	
	public static void main(String[] args){
		
		XThread obj = new XThread();
		Thread t = new Thread(obj);
		t.start();
		System.out.println(Thread.currentThread().getName());
	}
}
