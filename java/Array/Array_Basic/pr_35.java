import java.util.*;
class Code35 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter the value of k: ");
                int k = sc.nextInt();

                for(int i=0;i<arr.length-1;i++){
                         for(int j =0;j<arr.length-i-1;j++){
                                if(arr[j]>arr[j+1]){
                                        int temp = arr[j];
                                        arr[j]=arr[j+1];
                                        arr[j+1]=temp;
                                }
                        }
                }
                
                int result = 1;
                for (int i = 0; i < k; i++) {
                	
			result = (result * arr[i]);
                }
                
                System.out.println("output: "+result);
    
        }
}
