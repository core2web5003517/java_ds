import java.util.*;
class Code16 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                int n = arr.length;

                int count =0;
                for (int i = 0; i < n-1; i++) {
                         if (arr[i] > arr[(i + 1)]) {
                                count++;
                         }
                }
               System.out.println("Output: "+count);
        }

}
