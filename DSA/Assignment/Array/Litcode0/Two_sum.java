import java.util.*;

class Twosum{
    
    static int[] tosum(int arr[],int x){

           int indexes[]=new int[2];
           for (int i = 0; i < arr.length-1; i++) {

                    for (int j = i+1; j < arr.length; j++) {

                            if(arr[i]+arr[j]==x){

                                indexes[0]=i;
                                indexes[1]=j;
                            }
                    }
           }

        return indexes;
    }

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of array");
                int N = sc.nextInt();

                int arr[]=new int[N];

                System.out.println("Enter the elements in array");
                for (int i = 0; i < arr.length; i++) {

                        arr[i]=sc.nextInt();
                }

                System.out.println("Enter the target");
                int x = sc.nextInt();

                int ret[] = tosum(arr, x);

                for (int i = 0; i < ret.length; i++) {
                   
                         System.out.print(ret[i]+" ");
                }

                System.out.println();
        }
}
