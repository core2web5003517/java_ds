import java.io.*;
class Code5{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter the Number: ");
                int N = Integer.parseInt(br.readLine());
		int rev =0;
		while(N!=0){
			
			int rem = N%10;
			if(rem==0){
				
				rem=5;
			}
			rev = rev*10+rem;
			N = N/10;
		}
		int rev1 =0;
		while(rev!=0){
			
			int rem = rev%10;
			rev1=rev1*10+rem;
			rev = rev/10;
		}

		System.out.println(rev1);

	}
}
