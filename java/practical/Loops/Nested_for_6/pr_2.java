import java.io.*;

class NestedFor{

        public static void main(String[] args )throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Start :");
                int start=Integer.parseInt(br.readLine());

                System.out.println("Enter End :");
                int end=Integer.parseInt(br.readLine());

		System.out.println("Composite no ");

		for(int i=start;i<=end;i++){

			int num=i;

			int count=0;
			boolean flag=false;

			for(int j=1;j<=num;j++){

				if(num%j==0){

					count++;

					if(count>2){

						flag=true;
						break;
					}
				}
			}
			if(flag==true){

				System.out.println(i);
			}
		}
        }
}

