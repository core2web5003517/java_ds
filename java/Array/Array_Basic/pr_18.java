import java.util.*;
class Code18 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

                System.out.println("enter the targetsum: ");
                int targetSum = sc.nextInt();

                int n = arr.length;

                for (int i = 0; i < n; i++) {
                int currentSum = 0;

                	for (int j = i; j < n; j++) {
                 	currentSum = currentSum+arr[j];

                		if (currentSum == targetSum) {
                    		System.out.println("Sum found between indexes " + i + " and " + j);
                    		return;
                		}
            		}
       	 	}

        	System.out.println("No subarray found");
        }
}
