class Compute 
{
    public long[] minAnd2ndMin(long a[], long n)  
    {
        
        Arrays.sort(a);
        long[] arr=new long[2];
        long first=a[0];
        long second=-1;
        for(int i=1; i<n; i++){
            if(first != a[i]){
                second = a[i];
                break;
            }
        }
        if(second == -1){
            arr[0] = -1;
            return arr;
        }
        arr[0] = first;
        arr[1] = second;
        return arr;
    }
}

