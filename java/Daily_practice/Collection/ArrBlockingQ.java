import java.util.concurrent.*;
class ArrBQ {
	
	public static void main(String [] args)throws InterruptedException{
		
		BlockingQueue bq = new PriorityBlockingQueue(4);

		bq.put(10);
		bq.put(20);
		bq.put(5);
		bq.put(30);

		System.out.println(bq);

		bq.offer(40,4,TimeUnit.SECONDS);

		System.out.println(bq);
	}
}
