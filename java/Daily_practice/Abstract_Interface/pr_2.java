interface Demo{

       void fun();
       void gun();
}
class Child implements Demo{

       public void fun(){

                System.out.println("In child gun");
        }
	public void gun(){

                System.out.println("In par gun");
        }
}
class Client{

        public static void main(String[] args){

                Demo obj = new Child();
                obj.fun();
                obj.gun();
        }
}
