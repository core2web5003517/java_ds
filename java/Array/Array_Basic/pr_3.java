import java.util.*;
class Code3{
        public static void main(String [] args){
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                int largeNo=arr[0];
                for(int i=0;i<arr.length;i++){
                        if(arr[i]>largeNo){
                          largeNo=arr[i];
                        }
                }

                System.out.println("Large Number = "+largeNo);
	}
}
