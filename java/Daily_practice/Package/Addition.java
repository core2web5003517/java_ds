package arithfun;

public class Addition {
	
	static int num1 = 0;
	static int num2 = 0;

	public Addition(int num1, int num2){
		
		this.num1=num1;
		this.num2=num2;
	}
	public int add(){
		
		return num1+num2;
	}

	public int mult(){
		
		return num1*num2;
	}
	public static int sub(){
		
		return num1-num2;
	}
	public static double div(){
		
		return num1/num2;
	}
	public static int mod(){
		
		return num1%num2;
	}
}
