class Demo{
	
	private int x = 10;		
	Demo(){
		
		System.out.println("IN 1");
	}
	Demo(int x){
		
		  System.out.println("IN 2");
	}

	private void fun(){	

		  System.out.println("IN Fun");
	}

	public static void main(String[] args){

		Demo obj1 = new Demo();
		Demo obj2 = new Demo(20);
		obj1.fun();
	}
}
