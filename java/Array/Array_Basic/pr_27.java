import java.util.*;
class Code27 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of 1st Array : ");
                int N1 = sc.nextInt();
                
                int arr1[]=new int[N1];
                System.out.println("Enter the elements in 1st Array : ");
                
		for(int i=0;i<arr1.length;i++){
                        arr1[i]=sc.nextInt();
               	 	}

                System.out.println("Enter the size of 2nd Array : ");
                int N2 = sc.nextInt();
                
                int arr2[]=new int[N2];
                System.out.println("Enter the elements in 2nd Array : ");
                for(int i=0;i<arr2.length;i++){
                        arr2[i]=sc.nextInt();
                	}

                System.out.println("Enter the sum : ");
                int sum = sc.nextInt();
		
		int count =0;
		for(int i=0;i<arr1.length;i++){
			
			for(int j=0;j<arr2.length;j++){
				
				if(arr1[i]+arr2[j]==sum){
					
					count++;
				}
			}
		}
               
		System.out.println("Output: "+count);
                
           }
 }
