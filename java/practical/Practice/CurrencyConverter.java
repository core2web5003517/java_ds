import java.util.Scanner;

public class CurrencyConverter {
    public static void main(String[] args) {
        // Exchange rates
        double usdToEur = 0.89;
        double usdToGbp = 0.76;
        double usdToInr = 82.15;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter amount in USD: ");
        double usdAmount = scanner.nextDouble();

        System.out.println("Select currency to convert to:");
        System.out.println("1. EUR");
        System.out.println("2. GBP");
        System.out.println("3. INR");
        System.out.print("Enter your choice: ");
        int choice = scanner.nextInt();

        double convertedAmount = 0.0;

        switch (choice) {
            case 1:
                convertedAmount = usdAmount * usdToEur;
                System.out.printf("%.2f USD is equal to %.2f EUR \n", usdAmount, convertedAmount);
                break;
            case 2:
                convertedAmount = usdAmount * usdToGbp;
                System.out.printf("%.2f USD is equal to %.2f GBP \n", usdAmount, convertedAmount);
                break;
            case 3:
                convertedAmount = usdAmount * usdToInr;
                System.out.printf("%.2f USD is equal to %.2f INR \n", usdAmount, convertedAmount);
                break;
            default:
                System.out.println("Invalid choice");
                break;
        }

        scanner.close();
    }
}
