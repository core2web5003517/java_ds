class MyThread implements Runnable {
	
	public void run(){
		
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(10);
		}catch(InterruptedException ie){
		
		}
		System.out.println(Thread.currentThread().getThreadGroup().getName());
	}
}
class ThreadGPDemo{
	
	public static void main(String[] args){
		
		ThreadGroup PThread = new ThreadGroup("INDIA");
		
		MyThread obj1 = new MyThread();
		Thread t1 = new Thread(PThread,obj1,"Bihar");
		t1.start();

		MyThread obj2 = new MyThread();
		Thread t2 = new Thread(PThread,obj2,"Gujrat");
                t2.start();

		MyThread obj3 = new MyThread();
		Thread t3 = new Thread(PThread,obj3,"Maharashtra");
                t3.start();

		ThreadGroup CThread = new ThreadGroup(PThread,"Maharashtra");


               	MyThread obj4 = new MyThread();
                Thread t4 = new Thread(CThread,obj4,"Latur");
                t4.start();


		MyThread obj5 = new MyThread();
                Thread t5 = new Thread(CThread,obj5,"Pune");
                t5.start();

		MyThread obj6 = new MyThread();
                Thread t6 = new Thread(CThread,obj6,"Satara");
                t6.start();
	}
}
