import java.util.*;
class Code6{
        public static void main(String [] args){
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

		System.out.println("Enter the Range: ");

		int A = sc.nextInt();
		int B = sc.nextInt();

		int count =0;
		int expCount = B-A+1;

		for(int i=0;i<arr.length;i++){
			
			for(int j=A;j<=B;j++){
				
				if(arr[i]==j){
					
					count++;
				}
			}
		}

		if(count==expCount){
			
			System.out.println("yes");
		}else{
			
			System.out.println("No");
		}
	}
}
