import java.util.*;
class Demo1 implements Comparable<Demo1> {
	
	int no = 0;
	String name = null;

	Demo1( int no ,String name){
		
		this.no = no;
		this.name = name;
	}
	public String toString(){
		
		return name+"-"+no;
	}
	public int compareTo(Demo1 obj){
		
		return (this.name.compareTo(obj.name));
	}
}
class MainDemo{
	
	public static void main(String[]args){
		
		TreeSet<Demo1> al = new TreeSet<Demo1>();

		al.add(new Demo1(1,"shahu"));
		al.add(new Demo1(2,"abya"));
		al.add(new Demo1(3,"pp"));
		al.add(new Demo1(4,"nikya"));
		al.add(new Demo1(5,"akya"));
		al.add(new Demo1(6,"pp"));

		System.out.println(al);
	}
}
