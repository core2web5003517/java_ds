class Parent{
	
	Parent(){
		
		System.out.println("IN par constr");
	}
	int x = 10;
	int y= 20;
	static{
		
		  System.out.println("IN par static block");
	}
	void Method1(){
		
		  System.out.println("IN Method1");
	}
	static void Method2(){
		
		  System.out.println("IN Method2");
	}
	{
		  System.out.println("IN instance block");
	}

	public static void main(String[] args){
		
		Parent obj = new Parent();
		Method2();
		obj.Method1();
	}
}
