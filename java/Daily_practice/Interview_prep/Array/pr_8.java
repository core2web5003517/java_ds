import java.util.*;
class Code8{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Size of an Array :");
		int N = sc.nextInt();
		int Arr[] = new int[N];
		System.out.println("Enter the elements :");
		for(int i=0;i<N;i++){
			
			Arr[i]= sc.nextInt();
		}
		for(int i=0;i<N;i++){
			int freq=0;
			int num = Arr[i];
			for(int j=0;j<N;j++){
				
				if(num == Arr[j]){
					freq++;
				}
			}
			
			System.out.println("Frequency of "+num+ " is "+ freq);

		}
	}
}
