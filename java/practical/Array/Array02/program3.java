import java.io.*;
class Code3{
	public static void main(String[] args)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size of Array : ");
	int N = Integer.parseInt(br.readLine());
	int arr[]= new int [N];
	System.out.println("Enter the elements in array");
	int evensum=0;
	int oddsum=0;
	for(int i=0; i<arr.length; i++){
		arr[i]=Integer.parseInt(br.readLine());
		if(arr[i]%2==0){
		evensum=evensum+arr[i];
		}else{
		oddsum=oddsum+arr[i];
		}
	   }
		System.out.println("The sum of even elements is : " + evensum);
		System.out.println("The sum of odd elements is : " + oddsum);
	}
}

