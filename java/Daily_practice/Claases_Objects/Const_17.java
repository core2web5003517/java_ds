class Demo{

/*	Demo(){
		
		System.out.println("IN CONST_1");
	}*/
	Demo(int x){
		
		 System.out.println("IN CONST_2");
		 System.out.println(x);
	}
	Demo(Demo s){
		
		System.out.println("IN CONST_3");
		System.out.println(s);
	}
	 
        Demo(String str){

                 System.out.println("IN CONST_4");
                 System.out.println(str);
        }

	public static void main(String[] args){
		
		Demo obj3 = new Demo("Shahuraj");
	//	Demo obj = new Demo();
		Demo obj1 = new Demo(10);
		Demo obj2 = new Demo(obj1);
	}
}
