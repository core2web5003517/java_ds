class Solution {
 
    public static int firstRepeated(int[] arr, int n) {
        // Your code here
       int max = Integer.MIN_VALUE;
        for(int i =0; i<n; i++){
            if(arr[i]>max)
                max = arr[i];
        }
        
        
        int count[] = new int[max+1];
        for(int i = 0; i<n; i++){
            count[arr[i]] = count[arr[i]] +1;
   
        }
        
            
        
        for(int i = 0; i<n; i++){
            if(count[arr[i]] > 1)
                return i+1;
        }
        return -1;
            
    }
}

