class Code3{
	public static void main(String[] args){
		String str1="Shahu";
		String str2="Lohakare";
		
		System.out.println(str1+str2);
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		
		String str3= str1.concat(str2);
		
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		System.out.println(System.identityHashCode(str3));
	}
}
