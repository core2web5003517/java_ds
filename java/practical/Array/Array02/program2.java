import java.io.*;
class Code2{
	public static void main(String[] args)throws IOException{
	BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter the size of Array : ");
	int N = Integer.parseInt(br.readLine());
	int arr[]= new int [N];
	System.out.println("Enter the elements in array");
	int evenCount=0;
	int oddCount=0;
	for(int i=0; i<arr.length; i++){
		arr[i]=Integer.parseInt(br.readLine());
		if(arr[i]%2==0){
		evenCount++;
		}else{
		oddCount++;
		}
	   }
		System.out.println("The even count is : " + evenCount);
		System.out.println("The odd count is : " + oddCount);
	}
}

