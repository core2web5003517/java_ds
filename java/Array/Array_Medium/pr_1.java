import java.util.*;
class Code1 {
    public static void main(String[] args) {
                Scanner sc= new Scanner(System.in);
                System.out.println("Enter the size of Array : ");
                int N = sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter the elements in Array : ");
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }

               int max =arr[0];
               int index =0;

               for (int i = 0; i < arr.length; i++) {

                        if(arr[i]>max){

                                max = arr[i];
                                index = i;
                        }
               }

               System.out.println("Output: "+index);
        }
}
